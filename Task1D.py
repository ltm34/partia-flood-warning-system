from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
def run():

    stations = build_station_list()
    rivers = rivers_with_stations(stations)
    print(len(stations))
    rivers_sorted = sorted(rivers)
    for i in range(10):
        print(rivers_sorted[i])
    print(len(rivers_sorted))
    
    stations_in_river = stations_by_river(stations)
    print("River Aire: ")
    print(stations_in_river["River Aire"])
    print("River Cam")
    print(stations_in_river["River Cam"])
    print("River Thames")
    print(stations_in_river["River Severn"])
    
run()