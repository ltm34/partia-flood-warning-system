from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    stations = build_station_list()
    N = 9
    rivers = (rivers_by_station_number(stations, N))
    print(len(rivers))
    print(rivers)
    
run()