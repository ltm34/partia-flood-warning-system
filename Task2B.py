from floodsystem.utils import sorted_by_key 
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels


stations = build_station_list()
update_water_levels(stations)
L = stations_level_over_threshold(stations, 0.8)
for station in L:
    print("{}, {}".format(station[0], station[1]))