from floodsystem.utils import sorted_by_key 
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels


stations = build_station_list()
update_water_levels(stations)
L = stations_highest_rel_level(stations, 10)
for station in L:
    print("{}, {}".format(station[0], station[1]))