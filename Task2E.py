import operator
import datetime
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key
from floodsystem.station import relative_water_level
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    L = stations_highest_rel_level(stations, 5)
    for station_risk in L:
        for station in stations:
            if station_risk[0] == station.name:
                dates, levels = fetch_measure_levels(
                            station.measure_id, dt=datetime.timedelta(days=10))
                plot_water_levels(station, dates, levels)
run()