from floodsystem.analysis import stations_at_risk_number, clean_stations
from floodsystem.geo import stations_by_river, rivers_with_stations
from floodsystem.stationdata import build_station_list, update_water_levels

#building list of stations
stations = build_station_list()
update_water_levels(stations)

#build dictionary of towns and their stations
stations_in_river = stations_by_river(stations)

rivers = rivers_with_stations(stations)
rivers_severe = []
rivers_high = []
rivers_moderate = []
rivers_low = []

n = 0
for river in rivers:
    station_list = stations_in_river[river]
    station_object_list = []
    for station_string in station_list:
        for station in stations:
            if station.name == station_string:
                station_object_list.append(station)
    station_object_list = clean_stations(station_object_list)
    no_of_stations_at_risk = stations_at_risk_number(station_object_list)

    percent_at_risk = no_of_stations_at_risk/len(station_list)
    
    
    
    if percent_at_risk >= 0.7:
        rivers_severe.append(river)
    elif percent_at_risk <0.7 and percent_at_risk >= 0.5:
        rivers_high.append(river)
    elif percent_at_risk <0.3 and percent_at_risk >= 0.1:
        rivers_moderate.append(river)
    elif percent_at_risk < 0.1:
        rivers_low.append(river)
    print(river)

print(rivers_low)


town_severe = []
town_high = []
town_moderate = []
town_low = []

for station in stations:
    if station.river in rivers_severe:
        town_severe.append(station.town)
    elif station.river in rivers_high:
        town_high.append(station.town)
    elif station.river in rivers_moderate:
        town_moderate.append(station.town)
    elif station.river in rivers_low and station.town not in town_low:
        town_low.append(station.town)

print("Towns at severe risk: ")
print(set(town_severe))
print("Towns at high risk: ")
print(set(town_high))
print("Towns at moderate risk: ") 
print(set(town_moderate))
print("Towns at low risk: ")
print(town_low)