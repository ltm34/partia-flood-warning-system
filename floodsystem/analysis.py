import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import datetime
from floodsystem.datafetcher import fetch_measure_levels
def polyfit(dates, levels, p):
    date_num = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(date_num-date_num[0], levels, p)
    poly = np.poly1d(p_coeff)
    return poly, date_num[0]

def station_risk(station):
    p = 4
    dt = 2
    dates, levels = fetch_measure_levels(
                            station.measure_id, dt=datetime.timedelta(days=dt))
    if len(levels) != 0:
        poly, d0 = polyfit(dates, levels, p)
        dates_num = matplotlib.dates.date2num(dates)
        poly_dates = np.linspace(dates_num[0], dates_num[-1], 30)
        average_gradient = np.average(poly(poly_dates-d0))
        relative_level = station.relative_water_level()
        if relative_level != None:
            if average_gradient >0 and relative_level >= 1:
                return True
            else:
                return False
        else:
            return False
    else:
         return False

def clean_stations(stations):
    for station in stations:
        if not station.typical_range_consistent:
            stations.remove(station)
    return stations


def stations_at_risk_number(stations):
    n = 0
    for station in stations:
        if station_risk(station):
            n +=1
    return n



    