from floodsystem.utils import sorted_by_key 
from floodsystem.stationdata import update_water_levels

def stations_level_over_threshold(stations, tol):
    L = []
    n = 0
    for station in stations:
        relative_level = station.relative_water_level()
        if relative_level == None:
            n += 1
        elif relative_level > tol:
            S = (station.name, relative_level)
            L.append(S)
    return sorted_by_key(L, 1, True)

def stations_highest_rel_level(stations, N):
    L = stations_level_over_threshold(stations, 0)
    return L[0:N]


