# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from haversine import haversine, Unit
from floodsystem.utils import sorted_by_key  # noqa
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine, Unit


def rivers_with_stations(stations):
    rivers = []
    for station in stations:
        rivers.append(station.river)
    return set(rivers)


def stations_by_river(stations):
    rivers = rivers_with_stations(stations)
    rivers_dict = {}
    for river in rivers:
        L = []
        for station in stations:
            if station.river == river:
                L.append(station.name)
        L.sort()
        rivers_dict[river] = L

    return rivers_dict

def stations_by_river_objects(stations):
    rivers = rivers_with_stations(stations)
    rivers_dict = {}
    for river in rivers:
        L = []
        for station in stations:
            if station.river == river:
                L.append(station)
        rivers_dict[river] = L

    return rivers_dict

def rivers_by_station_number(stations, N):
    rivers = rivers_with_stations(stations)
    rivers_dict = stations_by_river(stations)
    L = []
    for river in rivers:
        numOfStations = len(rivers_dict[river])
        s = (river, numOfStations)
        L.append(s)
    L = sorted_by_key(L, 1)
    L.reverse()
    L_N =[]
    list_done = False
    i = 0
    while not list_done:
        L_N.append(L[i])
        if i == len(L) - 1:
            list_done = True
        elif i >= N:
            if L[i][1] != L[i+1][1]:
                list_done = True
        
        i +=1
    return L_N

def stations_by_distance (stations, p):
    L = []
    for station in stations:
        n = haversine(station.coord, p)
        S = (station.name, station.town, n)
        L.append(S)
    return sorted_by_key(L, 2)


def stations_within_radius(stations, centre, r):
    L = []
    S = stations_by_distance(stations, centre)
    for station in S:
        if station[2] <= r:
            L.append(station[0])
    return  sorted(L)