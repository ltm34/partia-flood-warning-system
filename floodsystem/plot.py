import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit
import numpy as np
def plot_water_levels(station, dates, levels):
    plt.plot(dates, levels, label = "Water level")
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    typical_range = station.typical_range
    high = [typical_range[1]]*len(dates)
    low = [typical_range[0]]*len(dates)

    plt.plot(dates, high, label = "Typical High")
    plt.plot(dates, low, label = "Typical Low")
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.legend()
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    #True plot
    dates_num = matplotlib.dates.date2num(dates)
    poly, d0 = polyfit(dates, levels, p)
    plt.plot(dates_num, levels, label = "True Water Level")
    
    #polynomial plot
    poly_dates = np.linspace(dates_num[0], dates_num[-1], 30)
    plt.plot(poly_dates, poly(poly_dates - d0), label = "Polynomial plot")
    
    #Typical high + low
    typical_range = station.typical_range
    high = [typical_range[1]]*len(dates)
    low = [typical_range[0]]*len(dates)
    plt.plot(dates, high, label = "Typical High")
    plt.plot(dates, low, label = "Typical Low")
    
    #Axis labels
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.legend()

    plt.show()