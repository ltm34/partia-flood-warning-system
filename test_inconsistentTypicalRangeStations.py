from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

# Create multiple stations
# Create a station
def test_inconsistentTypicanRangeStations():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (-2.3, 3.4445), "River X", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (5, 3.4445), "River Z", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River Y", "My Town")
    L = [test_stat_1, test_stat_2, test_stat_3]
    assert inconsistent_typical_range_stations(L) == ["y station", "z station"]