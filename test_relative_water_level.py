from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list


def test_():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (2, 4), "River X", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (2, 1), "River Z", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River Y", "My Town")
    test_stat_1.latest_level=3
    test_stat_2.latest_level=3
    test_stat_3.latest_level=3
    assert test_stat_1.relative_water_level() == 0.5
    assert test_stat_2.relative_water_level() == None
    assert test_stat_3.relative_water_level() == None
