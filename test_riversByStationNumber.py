from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def test_riversByStationNumber():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (-2.3, 3.4445), "A River", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (5, 3.4445), "A River", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "B River", "My Town")
    test_stat_4 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "C River", "My Town")
    L = [test_stat_1, test_stat_2, test_stat_3, test_stat_4]
    N = 2
    rivers = rivers_by_station_number(L, N)
    assert set(rivers) == {("A River", 2), ("B River", 1), ("C River", 1)}