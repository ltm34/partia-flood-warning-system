from floodsystem.geo import rivers_with_stations
from floodsystem.station import MonitoringStation
#test length of list created


def test_rivers_with_stations():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (-2.3, 3.4445), "River 1", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (5, 3.4445), "River 1", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River 2", "My Town")
    test_stat_4 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River 3", "My Town")
    L = [test_stat_1, test_stat_2, test_stat_3, test_stat_4]
    rivers = rivers_with_stations(L)
    assert rivers == {"River 1", "River 2", "River 3"}

test_rivers_with_stations()