from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def test_stations_by_distance():
    stations = build_station_list()
    p = (52.2053, 0.1218)
    assert stations_by_distance(stations, p)[0][:2] == ('Cambridge Jesus Lock', 'Cambridge')
    assert round (stations_by_distance(stations, p)[0][2],5)== 0.84024
 