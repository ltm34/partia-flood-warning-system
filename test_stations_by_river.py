from floodsystem.geo import stations_by_river
from floodsystem.station import MonitoringStation

#test that the river cam comes back with correct monitoring stations
def test_stations_by_river():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "Station A", (-2.0, 4.0), (-2.3, 3.4445), "River 1", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "Station B", (-2.0, 4.0), (5, 3.4445), "River 1", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","Station C", (-2.0, 4.0), None, "River 2", "My Town")
    test_stat_4 = MonitoringStation("test-s-id", "test-m-id","Station D", (-2.0, 4.0), None, "River 3", "My Town")
    L = [test_stat_1, test_stat_2, test_stat_3, test_stat_4]
    stationByRiver = stations_by_river(L)
    assert stationByRiver["River 1"] == ["Station A", "Station B"]