from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level


def test_():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (2, 4), "River X", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (2, 3), "River Z", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), (1,3), "River Y", "My Town")
    test_stat_4 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River Y", "My Town")
    test_stat_1.latest_level=3
    test_stat_2.latest_level=3
    test_stat_3.latest_level=3
    test_stat_4.latest_level=3
    L = [test_stat_1, test_stat_2, test_stat_3]
    assert stations_highest_rel_level(L, 1) == [('z station', 1.0)]
    assert stations_highest_rel_level(L, 2) == [('z station', 1.0), ('y station', 1.0)]
