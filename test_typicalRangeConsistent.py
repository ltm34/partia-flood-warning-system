from floodsystem.station import MonitoringStation
def test_typicalRangeConsistent():
    test_stat_1 = MonitoringStation("test-s-id", "test-m-id", "x station", (-2.0, 4.0), (-2.3, 3.4445), "River X", "My Town")
    test_stat_2 = MonitoringStation("test-s-id", "test-m-id", "z station", (-2.0, 4.0), (5, 3.4445), "River Z", "My Town")
    test_stat_3 = MonitoringStation("test-s-id", "test-m-id","y station", (-2.0, 4.0), None, "River Y", "My Town")
    assert test_stat_1.typical_range_consistent() == True
    assert test_stat_2.typical_range_consistent() == False
    assert test_stat_3.typical_range_consistent() == False